﻿declare var angular: any;

console.log("Angular is loading the app.");

class ReportController {
    static $inject = ["$scope", "$http"];
    constructor(
        private $scope,
        private $http
    ) {
        $scope.gridReady = false;
        
        console.log("Getting grid data...");

        $http.get('/api/QualifiedParts/Get').then(
            function (result) {
                $scope.mainGridOptions = {
                    dataSource: {
                        data: result.data,
                        schema: {
                            model: {
                                id: "PartNumber",
                                fields: {
                                    QplFlag: {},
                                    PartNumber: {},
                                    PartName: {},
                                    Revision: {},
                                    ToolDieSetNumber: {},
                                    OpenPo: {},
                                    SupplierCompanyName: {},
                                    SupplierCode: {},
                                    Jurisdication: {},
                                    Classification: {},
                                    Ctq: {},
                                    QplCreatedBy: {},
                                    QplCreatedDate: { type: "date" },
                                    QplLastUpdatedBy: {},
                                    QplLastUpdatedDate: { type: "date" },
                                    QplExpiresDate: { type: "date" }
                                }
                            }
                        }
                    },
                    pageable: {
                        pageSizes: [ 2, 5, 10, "all"]
                    },
                    columns: [
                        { field: "QplFlag", title: "QPL flag" },
                        { field: "PartNumber", title: "Part Number" },
                        { field: "PartName", title: "Part Name" },
                        { field: "Revision", title: "Revision" },
                        { field: "ToolDieSetNumber", title: "Tool Die Set Number" },
                        { field: "OpenPo", title: "Open PO flag" },
                        { field: "SupplierCompanyName", title: "Supplier" },
                        { field: "SupplierCode", title: "Supplier Code" },
                        { field: "Jurisdication", title: "Jurisdication" },
                        { field: "Classification", title: "Classification" },
                        { field: "Ctq", title: "CTQ Flag" },
                        { field: "QplCreatedBy", title: "QPL Created By" },
                        { field: "QplCreatedDate", title: "QPL Created Date", format: "{0:M/d/yy}" },
                        { field: "QplLastUpdatedBy", title: "QPL Last Updated By" },
                        { field: "QplLastUpdatedDate", title: "QPL Last Updated Date", format: "{0:M/d/yy}" },
                        { field: "QplExpiresDate", title: "QPL Expires Date", format: "{0:M/d/yy}" }
                            ]
                };
                $scope.gridReady = true;
                console.log("Grid created and data loaded.");
            }
        );        
    }
}

angular.module("Report", ["kendo.directives"])
	.controller("ReportController", ReportController);