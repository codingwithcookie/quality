﻿using System;

namespace Quality.Web.Models
{
    public class QualifiedPartsListModel
    {
        public bool QplFlag { get; set; }
        public string PartNumber { get; set; }
        public string PartName { get; set; }
        public string Revision { get; set; }
        public string ToolDieSetNumber { get; set; }
        public bool OpenPo { get; set; }
        public string SupplierCompanyName { get; set; }
        public string SupplierCode { get; set; }
        public string Jurisdication { get; set; }
        public string Classification { get; set; }
        public bool Ctq { get; set; }
        public int QplCreatedBy { get; set; }
        public DateTime QplCreatedDate { get; set; }
        public int QplLastUpdatedBy { get; set; }
        public DateTime QplLastUpdatedDate { get; set; }
        public DateTime? QplExpiresDate { get; set; }
    }
}