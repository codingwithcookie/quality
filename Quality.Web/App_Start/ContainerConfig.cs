﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using NLog;
using Quality.Web.Repository;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;

namespace Quality.Web.App_Start
{
    public class ContainerConfig
    {
        public static IContainer Container;
        public static void RegisterComponents()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<QualifiedPartsListRepository>().As<IQualifiedPartsListRepository>();
            builder.Register(x => LogManager.GetCurrentClassLogger()).As<ILogger>();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var config = GlobalConfiguration.Configuration;
            Container = builder.Build();
            
            var dependencyResolver = new AutofacDependencyResolver(Container);
            DependencyResolver.SetResolver(dependencyResolver);

            config.DependencyResolver = new AutofacWebApiDependencyResolver(Container);
        }
    }
}