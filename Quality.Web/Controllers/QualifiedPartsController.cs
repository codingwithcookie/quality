﻿using Quality.Web.Models;
using Quality.Web.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Quality.Web.Controllers
{
    public class QualifiedPartsController : ApiController
    {
        private readonly IQualifiedPartsListRepository _qualifiedPartsListRepository;
        public QualifiedPartsController(IQualifiedPartsListRepository qualifiedPartsListRepository)
        {
            _qualifiedPartsListRepository = qualifiedPartsListRepository;
        }
        
        public async Task<List<QualifiedPartsListModel>> Get()
        {
            var result = await _qualifiedPartsListRepository.GetQualifiedPartsList();
            return result;
        }
    }
}