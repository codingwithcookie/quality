﻿using NLog;
using Quality.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Quality.Web.Repository
{
    public interface IQualifiedPartsListRepository
    {
        Task<List<QualifiedPartsListModel>> GetQualifiedPartsList();
    }

    public class QualifiedPartsListRepository : IQualifiedPartsListRepository
    {
        private static ILogger _log;
        private static string SqlConnectionString => ConfigurationManager.ConnectionStrings["qualityContext"].ConnectionString ?? "";

        public QualifiedPartsListRepository(ILogger log)
        {
            _log = log;
        }

        public async Task<List<QualifiedPartsListModel>> GetQualifiedPartsList()
        {
            var storedProcedureName = "usp_GetAllQualifiedParts";
            try
            {                
                using (var db = new SqlConnection(SqlConnectionString))
                {
                    using (var cmd = new SqlCommand(storedProcedureName, db))
                    {
                        db.Open();
                        var dt = new DataTable();
                        dt.Load(await cmd.ExecuteReaderAsync());                        

                        var result = dt.Rows.OfType<DataRow>().Select(row => new QualifiedPartsListModel
                        {
                            QplFlag = row.Field<bool>("QplFlag"),
                            PartNumber = row.Field<string>("PartNumber"),
                            PartName = row.Field<string>("PartName"),
                            Revision = row.Field<string>("Revision"),
                            ToolDieSetNumber = row.Field<string>("ToolDieSetNumber"),
                            OpenPo = row.Field<bool>("OpenPo"),
                            SupplierCompanyName = row.Field<string>("SupplierCompanyName"),
                            SupplierCode = row.Field<string>("SupplierCode"),
                            Jurisdication = row.Field<string>("Jurisdication"),
                            Classification = row.Field<string>("Classification"),
                            Ctq = row.Field<bool>("Ctq"),
                            QplCreatedBy = row.Field<int>("QplCreatedBy"),
                            QplCreatedDate = row.Field<DateTime>("QplCreatedDate"),
                            QplLastUpdatedBy = row.Field<int>("QplLastUpdatedBy"),
                            QplLastUpdatedDate = row.Field<DateTime>("QplLastUpdatedDate"),
                            QplExpiresDate = row.Field<DateTime?>("QplExpiresDate")
                        }).ToList();

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex, $"IQualifiedPartsListRepository.GetQualifiedPartsList failed. Error: {ex.Message}");
                return null;
            }
        }
    }
}